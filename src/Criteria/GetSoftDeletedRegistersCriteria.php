<?php

namespace Menezes\CrudGenerator\Criteria;

use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class GetSoftDeletedRegistersCriteria.
 *
 * @package namespace App\Criteria;
 */
class GetSoftDeletedRegistersCriteria implements CriteriaInterface
{
    /**
     * @var Request
     */
    protected $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Apply criteria in query repository
     *
     * @param Builder|Model $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     * @throws Exception
     */
    public function apply($model, RepositoryInterface $repository)
    {
        $macro = $this->request->get(config('repository.criteria.params.macro', 'macro'), null);

        if (isset($macro) && !empty($macro)) {
            switch ($macro) {
                case 'withTrashed':
                    $model = $model->withTrashed();
                    break;
                case 'onlyTrashed':
                    $model = $model->onlyTrashed();
                    break;
                default:
                    break;
            }
        }

        return $model;
    }
}
