<?php


namespace Menezes\CrudGenerator\Common;


use Illuminate\Support\Str;

class GeneratorConfig
{
    /* Namespace variables */
    public $nsApp;
    public $nsRepository;
    public $nsService;
    public $nsTransformer;
    public $nsPresenter;
    public $nsModel;
    public $nsModelRelation;
    public $nsModelExtend;
    public $nsApiController;
    public $nsApiRequest;
    public $nsRequestBase;
    public $nsBaseController;
    public $nsPolicy;
    public $nsModelUser;

    /* Path variables */
    public $pathRepository;
    public $pathModel;
    public $pathService;
    public $pathPresenter;
    public $pathTransformer;
    public $pathApiController;
    public $pathApiRequest;
    public $pathApiRoutes;
    public $pathPolicy;
    public $pathUser;

    /* Model Names */
    public $mName;
    public $mPlural;
    public $mCamel;
    public $mCamelPlural;
    public $mSnake;
    public $mSnakePlural;
    public $mDashed;
    public $mDashedPlural;
    public $mSlash;
    public $mSlashPlural;
    public $mHuman;
    public $mHumanPlural;
    public $mUppercase;
    public $mUser;

    /* Generator Options */
    public $options;

    /** @var CommandData */
    private $commandData;

    /* Command Options */
    public static $availableOptions = [
        'tableName',
        'fromTable',
        'ignoreFields',
        'softDelete',
        'plural'
    ];

    public $tableName;

    public function init(CommandData $commandData)
    {
        $this->mName = $commandData->modelName;
        $this->prepareOptions($commandData);
        $this->prepareModelNames();
        $this->loadPaths();
        $this->loadNamespaces($commandData);
        $this->loadDynamicVariables($commandData);
    }

    public function prepareOptions(CommandData $commandData)
    {
        foreach (self::$availableOptions as $option) {
            $this->options[$option] = $commandData->commandObj->option($option);
        }

        if ($this->options['fromTable'] && !$this->options['tableName']) {
            $commandData->commandError('tableName required with fromTable option.');
            exit;
        }

        $this->tableName = $this->getOption('tableName');

        $this->options['softDelete'] = config('menezes.laravel_generator.options.softDelete', true);
    }

    public function prepareModelNames()
    {
        if ($this->getOption('plural')) {
            $this->mPlural = $this->getOption('plural');
        } else {
            $this->mPlural = Str::plural($this->mName);
        }

        $this->mCamel = Str::camel($this->mName);
        $this->mCamelPlural = Str::camel($this->mPlural);
        $this->mSnake = Str::snake($this->mName);
        $this->mSnakePlural = Str::snake($this->mPlural);
        $this->mDashed = str_replace('_', '-', Str::snake($this->mSnake));
        $this->mDashedPlural = str_replace('_', '-', Str::snake($this->mSnakePlural));
        $this->mSlash = str_replace('_', '/', Str::snake($this->mSnake));
        $this->mSlashPlural = str_replace('_', '/', Str::snake($this->mSnakePlural));
        $this->mHuman = Str::title(str_replace('_', ' ', Str::snake($this->mSnake)));
        $this->mHumanPlural = Str::title(str_replace('_', ' ', Str::snake($this->mSnakePlural)));
        $this->mUppercase = Str::upper($this->mName);
        $this->mUser = config('menezes.laravel_generator.model_user', 'Usuario');
    }

    public function getOption($option)
    {
        return $this->options[$option] ?? false;
    }

    public function loadPaths()
    {
        $this->pathRepository = config('menezes.laravel_generator.path.repository', app_path('Repositories/'));

        $this->pathModel = config('menezes.laravel_generator.path.model', app_path('Models/'));

        $this->pathService = config('menezes.laravel_generator.path.service', app_path('Services/'));

        $this->pathPresenter = config('menezes.laravel_generator.path.presenter', app_path('Presenters/'));

        $this->pathTransformer = config('menezes.laravel_generator.path.transformer', app_path('Transformers/'));

        $this->pathApiController = config('menezes.laravel_generator.path.controller', app_path('Http/Controllers/'));

        $this->pathApiRequest = config('menezes.laravel_generator.path.request', app_path('Http/Requests/'));

        $this->pathApiRoutes = config('menezes.laravel_generator.path.routes', base_path('routes/api.php'));

        $this->pathPolicy = config('menezes.laravel_generator.path.policy', app_path('Policies/'));

        $this->pathUser = config('menezes.laravel_generator.path.user_model', app_path('Models/Seguranca'));
    }

    public function loadNamespaces(CommandData $commandData)
    {
        $this->nsApp = $commandData->commandObj->getLaravel()->getNamespace();
        $this->nsApp = substr($this->nsApp, 0, -1);

        $this->nsModel = config('menezes.laravel_generator.namespace.model', 'App\Models');
        $this->nsApiRequest = config('menezes.laravel_generator.namespace.request', 'App\Http\Requests');
        $this->nsRepository = config('menezes.laravel_generator.namespace.repository', 'App\Repositories');
        $this->nsService = config('menezes.laravel_generator.namespace.service', 'App\Services');
        $this->nsTransformer = config('menezes.laravel_generator.namespace.transformer', 'App\Transformers');
        $this->nsPresenter = config('menezes.laravel_generator.namespace.presenter', 'App\Presenters');
        $this->nsApiController = config('menezes.laravel_generator.namespace.controller', 'App\Http\Controllers');
        $this->nsPolicy = config('menezes.laravel_generator.namespace.policy', 'App\Policies');

        $this->nsModelRelation = config('menezes.laravel_generator.namespace.model', 'App\Models');
        $this->nsModelUser = config('menezes.laravel_generator.namespace.user_model', 'App\Models\Seguranca');
        $this->nsModelExtend = config('menezes.laravel_generator.model_extend_class', 'Illuminate\Database\Eloquent\Model');
        $this->nsRequestBase = config('menezes.laravel_generator.namespace.request', 'App\Http\Requests');
        $this->nsBaseController = config('menezes.laravel_generator.namespace.controller', 'App\Http\Controllers');

        $modelNamespace = explode('.', $this->tableName);
        // Checa se tem uma schema. Ex: cadastro.usuario
        if (count($modelNamespace) > 1) {
            // Se tiver, o namespace é cadastro
            $modelNamespace = Str::ucfirst($modelNamespace[0]);
            $this->nsModel .= "\\$modelNamespace";
            $this->nsApiRequest .= "\\$modelNamespace";
            $this->nsRepository .= "\\$modelNamespace";
            $this->nsService .= "\\$modelNamespace";
            $this->nsTransformer .= "\\$modelNamespace";
            $this->nsPresenter .= "\\$modelNamespace";
            $this->nsApiController .= "\\$modelNamespace";
            $this->nsPolicy .= "\\$modelNamespace";
        }
    }

    public function loadDynamicVariables(CommandData $commandData)
    {
        $commandData->addDynamicVariable('$NAMESPACE_APP$', $this->nsApp);
        $commandData->addDynamicVariable('$NAMESPACE_REPOSITORY$', $this->nsRepository);
        $commandData->addDynamicVariable('$NAMESPACE_SERVICE$', $this->nsService);
        $commandData->addDynamicVariable('$NAMESPACE_TRANSFORMER$', $this->nsTransformer);
        $commandData->addDynamicVariable('$NAMESPACE_PRESENTER$', $this->nsPresenter);
        $commandData->addDynamicVariable('$NAMESPACE_MODEL$', $this->nsModel);
        $commandData->addDynamicVariable('$NAMESPACE_MODEL_RELATION$', $this->nsModelRelation);
        $commandData->addDynamicVariable('$NAMESPACE_MODEL_EXTEND$', $this->nsModelExtend);
        $commandData->addDynamicVariable('$NAMESPACE_API_CONTROLLER$', $this->nsApiController);
        $commandData->addDynamicVariable('$NAMESPACE_API_REQUEST$', $this->nsApiRequest);
        $commandData->addDynamicVariable('$NAMESPACE_BASE_CONTROLLER$', $this->nsBaseController);
        $commandData->addDynamicVariable('$NAMESPACE_REQUEST_BASE$', $this->nsRequestBase);
        $commandData->addDynamicVariable('$NAMESPACE_POLICY$', $this->nsPolicy);
        $commandData->addDynamicVariable('$NAMESPACE_USER$', $this->nsModelUser);

        $commandData->addDynamicVariable('$TABLE_NAME$', $this->tableName);
        $commandData->addDynamicVariable('$TABLE_NAME_TITLE$', Str::studly($this->tableName));

        $commandData->addDynamicVariable('$MODEL_NAME$', Str::studly($this->mName));
        $commandData->addDynamicVariable('$MODEL_NAME_CAMEL$', $this->mCamel);
        $commandData->addDynamicVariable('$MODEL_NAME_PLURAL$', $this->mPlural);
        $commandData->addDynamicVariable('$MODEL_NAME_PLURAL_CAMEL$', $this->mCamelPlural);
        $commandData->addDynamicVariable('$MODEL_NAME_SNAKE$', $this->mSnake);
        $commandData->addDynamicVariable('$MODEL_NAME_PLURAL_SNAKE$', $this->mSnakePlural);
        $commandData->addDynamicVariable('$MODEL_NAME_DASHED$', $this->mDashed);
        $commandData->addDynamicVariable('$MODEL_NAME_PLURAL_DASHED$', $this->mDashedPlural);
        $commandData->addDynamicVariable('$MODEL_NAME_SLASH$', $this->mSlash);
        $commandData->addDynamicVariable('$MODEL_NAME_PLURAL_SLASH$', $this->mSlashPlural);
        $commandData->addDynamicVariable('$MODEL_NAME_HUMAN$', $this->mHuman);
        $commandData->addDynamicVariable('$MODEL_NAME_PLURAL_HUMAN$', $this->mHumanPlural);
        $commandData->addDynamicVariable('$MODEL_NAME_UPPERCASE$', $this->mUppercase);
        $commandData->addDynamicVariable('$MODEL_USER$', $this->mUser);

        $this->commandData = $commandData;
    }
}
