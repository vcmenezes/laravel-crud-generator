<?php

namespace Menezes\CrudGenerator\Common;

use Doctrine\DBAL\DBALException;
use Illuminate\Console\Command;
use Menezes\CrudGenerator\Utils\TableFieldsGenerator;

class CommandData
{
    /** @var string */
    public $modelName;

    /** @var GeneratorConfig */
    public $config;

    /** @var GeneratorField[] */
    public $fields = [];

    /** @var GeneratorFieldRelation[] */
    public $relations = [];

    /** @var Command */
    public $commandObj;

    /** @var array */
    public $dynamicVars = [];

    /** @var TableFieldsGenerator */
    public $tableFieldsGenerator;

    /**
     * @param Command $commandObj
     */
    public function __construct(Command $commandObj)
    {
        $this->commandObj = $commandObj;
        $this->config = new GeneratorConfig();
    }

    public function initCommandData()
    {
        $this->config->init($this);
    }

    /**
     * @throws DBALException
     */
    public function getFields()
    {
        $this->fields = [];

        if ($this->getOption('fromTable')) {
            $this->getInputFromTable();
        }
    }

    public function commandError($error)
    {
        $this->commandObj->error($error);
    }

    public function commandComment($message)
    {
        $this->commandObj->comment($message);
    }

    public function commandWarn($warning)
    {
        $this->commandObj->warn($warning);
    }

    public function commandInfo($message)
    {
        $this->commandObj->info($message);
    }

    public function addDynamicVariable($name, $val)
    {
        $this->dynamicVars[$name] = $val;
    }

    public function getOption($option)
    {
        return $this->config->getOption($option);
    }

    /**
     * @throws DBALException
     */
    private function getInputFromTable()
    {
        $tableName = $this->dynamicVars['$TABLE_NAME$'];

        $ignoredFields = $this->getOption('ignoreFields');

        if (!empty($ignoredFields)) {
            $ignoredFields = explode(',', trim($ignoredFields));
        } else {
            $ignoredFields = [];
        }

        $this->tableFieldsGenerator = new TableFieldsGenerator($tableName, $ignoredFields);
        $this->tableFieldsGenerator->prepareFieldsFromTable();
        $this->tableFieldsGenerator->prepareRelations();

        $this->fields = $this->tableFieldsGenerator->fields;
        $this->relations = $this->tableFieldsGenerator->relations;
    }
}
