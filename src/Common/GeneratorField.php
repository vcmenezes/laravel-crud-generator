<?php


namespace Menezes\CrudGenerator\Common;


class GeneratorField
{
    /** @var string */
    public $name;
    public $dbInput;
    public $fieldType;
    public $description;

    /** @var string */
    public $migrationText;
    public $foreignKeyText;
    public $validations;

    /** @var bool */
    public $isFillable = true;
    public $isNotNull = false;

    public function parseDBType($dbInput)
    {
        $this->dbInput = $dbInput;
        $this->prepareMigrationText();
    }

    // integer:unsigned:nullable:foreign,posts,id
    private function prepareMigrationText()
    {
        $inputsArr = explode(':', $this->dbInput);
        $this->migrationText = '$table->';

        $fieldTypeParams = explode(',', array_shift($inputsArr));
        $this->fieldType = array_shift($fieldTypeParams);
        $this->migrationText .= $this->fieldType . "('" . $this->name . "'";

        if ($this->fieldType === 'enum') {
            $this->migrationText .= ', [';
            foreach ($fieldTypeParams as $param) {
                $this->migrationText .= "'" . $param . "',";
            }
            $this->migrationText = substr($this->migrationText, 0, -1);
            $this->migrationText .= ']';
        } else {
            foreach ($fieldTypeParams as $param) {
                $this->migrationText .= ', ' . $param;
            }
        }

        $this->migrationText .= ')';

        foreach ($inputsArr as $input) {
            $inputParams = explode(',', $input);
            $functionName = array_shift($inputParams);
            if ($functionName === 'foreign') {
                $fkName = array_shift($inputParams);
                $foreignTable = array_shift($inputParams);
                $foreignField = array_shift($inputParams);
                $onUpdate = array_shift($inputParams);
                $onDelete = array_shift($inputParams);
                $this->foreignKeyText .= "\$table->index(['$this->name'], '$this->name" . "_idx');" . infy_nl_tab(1, 3);
                $this->foreignKeyText .= "\$table->foreign('" . $this->name . "', '" . $fkName . "')" . infy_nl_tab(1, 4) .
                    "->references('" . $foreignField . "')->on('" . $foreignTable . "')";
                $this->foreignKeyText .= $onUpdate ? infy_nl_tab(1, 4) . "->onUpdate('" . $onUpdate . "')" : '';
                $this->foreignKeyText .= $onDelete ? infy_nl_tab(1, 4) . "->onDelete('" . $onDelete . "')" : '';
                $this->foreignKeyText .= ';' . infy_nl_tab(1, 3);
            } else {
                $this->migrationText .= '->' . $functionName;
                $this->migrationText .= $functionName === 'comment' ? "('" : '(';
                $this->migrationText .= implode(', ', $inputParams);
                $this->migrationText .= $functionName === 'comment' ? "')" : ')';
            }
        }

        $this->migrationText .= ';';

        if ($this->name === 'deleted_at') {
            $this->migrationText = '$table->softDeletes();';
        }

        $this->migrationText .= infy_nl_tab(1, 3);
    }
}
