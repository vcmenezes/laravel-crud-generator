<?php

namespace Menezes\CrudGenerator\Common;

use Illuminate\Support\Str;

class GeneratorFieldRelation
{
    /** @var string */
    public $type;
    public $inputs;
    public $localField;
    public $relationNameSingular;
    public $relationNamePlural;
    public $namespace;

    public static function parseRelation($relationInput, $localField = null)
    {
        $inputs = explode(',', $relationInput);
        $relation = new self();
        $relation->type = array_shift($inputs);
        $relation->inputs = $inputs;
        $relation->localField = $localField;
        [$relation->relationNameSingular, $relation->relationNamePlural, $relation->namespace] = $relation->getRelationIncludeName();
        return $relation;
    }

    public function getRelationIncludeName()
    {
        if (!empty($this->inputs[0])) {
            $singularRelation = Str::camel($this->inputs[0]);
            $pluralRelation = Str::camel(Str::plural($this->inputs[0]));
            $namespace = '';
        } else {
            return false;
        }

        if (strpos($singularRelation, '.') !== false) {
            $namespace = explode('.', $singularRelation)[0];
            $singularRelation = explode('.', $singularRelation)[1];
            $pluralRelation = explode('.', $pluralRelation)[1];
        }

        return [$singularRelation, $pluralRelation, $namespace];
    }

    public function getRelationFunctionName()
    {
        [$singularRelation, $pluralRelation] = $this->getRelationIncludeName();

        switch ($this->type) {
            case '1t1':
//                if (isset($this->inputs[1])) {
//                    $singularRelation = Str::camel(str_replace('id_', '', strtolower($this->inputs[1])));
//                }
                $functionName = $singularRelation;
                $relation = 'hasOne';
                $relationClass = 'HasOne';
                break;
            case '1tm':
                $functionName = $pluralRelation;
                $relation = 'hasMany';
                $relationClass = 'HasMany';
                break;
            case 'mt1':
                //TODO rever pois foi comentado por causa do conflito de nomes gerados
//                if (isset($this->inputs[1])) {
//                    $singularRelation = Str::camel(str_replace('id_', '', strtolower($this->inputs[1])));
//                }
                $functionName = $singularRelation;
                $relation = 'belongsTo';
                $relationClass = 'BelongsTo';
                break;
            case 'mtm':
                $functionName = $pluralRelation;
                $relation = 'belongsToMany';
                $relationClass = 'BelongsToMany';
                break;
            case 'hmt':
                $functionName = $pluralRelation;
                $relation = 'hasManyThrough';
                $relationClass = 'HasManyThrough';
                break;
            default:
                $functionName = '';
                $relation = '';
                $relationClass = '';
                break;
        }

        return [$functionName, $relation, $relationClass];
    }

    public function getRelationFunctionText()
    {
        [$functionName, $relation, $relationClass] = $this->getRelationFunctionName();

        if (!empty($functionName) && !empty($relation)) {
            return $this->generateRelation($functionName, $relation, $relationClass);
        }

        return false;
    }

    private function generateRelation($functionName, $relation, $relationClass)
    {
        $inputs = $this->inputs;
        $relationModelName = array_shift($inputs);
        $relationModelNameArray = explode('.', $relationModelName);
        if (count($relationModelNameArray) > 1) {
            $relationModelName = Str::ucfirst($relationModelNameArray[0]) . '\\' . Str::ucfirst($relationModelNameArray[1]);
        } else {
            $relationModelName = Str::ucfirst($relationModelNameArray[0]);
        }

        $template = get_template('model.relationship', 'crud-generator');

        $template = str_replace('$RELATIONSHIP_CLASS$', $relationClass, $template);
        $template = str_replace('$FUNCTION_NAME$', $functionName, $template);
        $template = str_replace('$RELATION$', $relation, $template);
        $template = str_replace('$RELATION_MODEL_NAME$', $relationModelName, $template);

        $inputFields = '';
        foreach ($inputs as $input) {
            $inputFields .= ', self::' . Str::upper($input);
        }

        if ($this->localField && empty($inputFields)) {
            $inputFields .= ', ' . Str::ucfirst($relationModelNameArray[1]) . '::' . Str::upper($this->localField);
        }

        $template = str_replace('$INPUT_FIELDS$', $inputFields, $template);

        return [$functionName, $template];
    }
}
