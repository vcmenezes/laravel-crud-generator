<?php


namespace Menezes\CrudGenerator\Generators;

use Doctrine\DBAL\Schema\AbstractSchemaManager;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class AllBaseGenerator
{
    private $tables;

    /** @var Command $command */
    private $command;

    public function __construct(Command $command)
    {
        $this->command = $command;
        /** @var AbstractSchemaManager $schemaManager */
        $schemaManager = DB::getDoctrineSchemaManager();
        $this->tables = $schemaManager->listTableNames();
    }

    public function generate($command)
    {
        $bar = $this->command->getOutput()->createProgressBar(count($this->tables));
        $bar->start();

        foreach ($this->tables as $table) {
            if (strpos($table, '.') !== false) {
                $modelName = Str::ucfirst(explode('.', $table)[1]);
            } else {
                $modelName = Str::ucfirst($table);
            }

            Artisan::call("$command $modelName --fromTable --tableName=$table");

            $bar->advance();
        }
        $bar->finish();
    }
}
