<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllRequestsGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generateRequests()
    {
        $this->generate('create:api_request');
    }
}
