<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllServicesGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generateServices()
    {
        $this->generate('create:api_service');
    }
}
