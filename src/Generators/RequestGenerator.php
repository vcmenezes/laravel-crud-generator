<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;

class RequestGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;

    /** @var string */
    private $createFileName;

    /** @var string */
    private $updateFileName;

    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;
        $this->path = $commandData->config->pathApiRequest;

        $tableName = $this->commandData->config->tableName;
        if (strpos($tableName, '.') !== false) {
            $this->path .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
        }

        $this->createFileName = 'Create' . $this->commandData->modelName . 'Request.php';
        $this->updateFileName = 'Update' . $this->commandData->modelName . 'Request.php';
    }

    public function generate()
    {
        $this->generateCreateRequest();
        $this->generateUpdateRequest();
    }

    private function generateCreateRequest()
    {
        $templateData = get_template('api.request.create_request', 'crud-generator');

        $templateData = str_replace('$RULES$', implode(',' . infy_nl_tab(1, 3), $this->generateCreateRules()), $templateData);

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        FileUtil::createFile($this->path, $this->createFileName, $templateData);

        $this->commandData->commandComment("\nCreate Request created: ");
        $this->commandData->commandInfo($this->createFileName);
    }

    private function generateUpdateRequest()
    {
        $templateData = get_template('api.request.update_request', 'crud-generator');

        $templateData = str_replace('$RULES$', implode(',' . infy_nl_tab(1, 3), $this->generateUpdateRules()), $templateData);

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        FileUtil::createFile($this->path, $this->updateFileName, $templateData);

        $this->commandData->commandComment("\nUpdate Request created: ");
        $this->commandData->commandInfo($this->updateFileName);
    }

    private function generateCreateRules()
    {
        $rules = [];

        $removeFields = ['id', 'created_at', 'updated_at', 'deleted_at'];
        $fields = array_filter($this->commandData->fields, function ($field) use ($removeFields) {
            return !in_array($field->name, $removeFields, true);
        });

        foreach ($fields as $field) {
            if ($field->isNotNull && empty($field->validations)) {
                $field->validations = 'required|';
            }
            if (!empty($field->validations)) {
                $rule = $this->commandData->modelName . '::' . Str::upper($field->name) . " => '$field->validations";
                $rule .= $this->getRulesFromField($field->dbInput);
            } else {
                $rule = $this->commandData->modelName . '::' . Str::upper($field->name) . " => '";
                $rule .= $this->getRulesFromField($field->dbInput);
            }

            $rule .= "'";

            $rules[] = $rule;
        }

        return $rules;
    }

    private function generateUpdateRules()
    {
        $rules = [];

        $removeFields = ['id', 'created_at', 'updated_at', 'deleted_at'];
        $fields = array_filter($this->commandData->fields, function ($field) use ($removeFields) {
            return !in_array($field->name, $removeFields, true);
        });

        foreach ($fields as $field) {
            $rule = $this->commandData->modelName . '::' . Str::upper($field->name) . " => '";
            $rule .= $this->getRulesFromField($field->dbInput);
            $rule .= "'";
            $rules[] = $rule;
        }

        return $rules;
    }

    private function getRulesFromField($field)
    {
        //ex: dbInput = string,20:nullable
        if (Str::contains($field, ['bigInteger', 'integer'])) {
            $inputParams = explode(',', $field);
            if (isset($inputParams[0])) {
                $params = explode(':', $inputParams[0]);
                if (isset($inputParams[2]) && in_array('foreign', $params, true)) {
                    $foreignTable = $inputParams[2];
                    return "integer|exists:$foreignTable,id";
                }
            }
            return 'integer';
        }

        if (Str::contains($field, 'boolean')) {
            return 'boolean';
        }

        if (Str::contains($field, ['datetime', 'date'])) {
            return 'date';
        }

        if (Str::contains($field, 'string')) {
            $rules = explode(':', $field);

            $maxSize = explode(',', $rules[0]);
            if (count($maxSize) === 1) {
                $maxSize = 255;
            } else {
                $maxSize = $maxSize[1];
            }
            $stringRule = 'string|max:' . $maxSize;

            if ((count($rules) > 1) && Str::contains($rules[1], 'comment')) {
                $comment = $rules[1];
                return $stringRule . '|in:' . Str::replaceFirst('comment,', '', $comment);
            }

            return $stringRule;
        }

        return null;
    }
}
