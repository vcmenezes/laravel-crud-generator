<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;

class RepositoryGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $pathRepository;
    private $pathInterface;

    /** @var string */
    private $fileNameRepository;
    private $fileNameInterface;

    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;
        $this->pathRepository = $commandData->config->pathRepository;
        $this->pathInterface = $commandData->config->pathRepository;

        $tableName = $this->commandData->config->tableName;
        if (strpos($tableName, '.') !== false) {
            $this->pathRepository .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
            $this->pathInterface .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
        }

        $this->pathRepository .= 'Repositories/';
        $this->pathInterface .= 'Interfaces/';

        $this->fileNameRepository = $this->commandData->modelName . 'RepositoryEloquent.php';
        $this->fileNameInterface = $this->commandData->modelName . 'Repository.php';
    }

    public function generate()
    {
        $templateDataRepository = get_template('repository', 'crud-generator');
        $templateDataInterface = get_template('interface_repository', 'crud-generator');

        $templateDataRepository = fill_template($this->commandData->dynamicVars, $templateDataRepository);
        $templateDataInterface = fill_template($this->commandData->dynamicVars, $templateDataInterface);

        FileUtil::createFile($this->pathRepository, $this->fileNameRepository, $templateDataRepository);
        FileUtil::createFile($this->pathInterface, $this->fileNameInterface, $templateDataInterface);

        $this->makeBinding();

        $this->commandData->commandComment("\nRepository created: ");
        $this->commandData->commandInfo($this->fileNameRepository);
        $this->commandData->commandComment("\nInterface created: ");
        $this->commandData->commandInfo($this->fileNameInterface);
    }

    private function makeBinding()
    {
        try {
            // generate repository service provider
            $templateData = get_template('bindings', 'crud-generator');

            $bindPlaceholder = '//:end-bindings:';
            $bindNamespace = '//:binding-namespace:';

            $path = config('menezes.laravel_generator.path.provider', app_path('Providers/'));
            $fileName = 'RepositoryServiceProvider.php';

            if (!file_exists($path . $fileName)) {
                FileUtil::createFile($path, $fileName, $templateData);
            }

            // Add entity repository binding to the repository service provider
            $providerFile = File::get($path . $fileName);
            $modelName = $this->commandData->modelName;

            $repositoryInterface = $modelName . 'Repository::class';
            $repositoryEloquent = $modelName . 'RepositoryEloquent::class';

            $templateData = str_replace($bindPlaceholder, "\$this->app->bind({$repositoryInterface}, $repositoryEloquent);" . PHP_EOL . '        ' . $bindPlaceholder, $providerFile);

            $repositoryNamespace = 'use ' . $this->commandData->dynamicVars['$NAMESPACE_REPOSITORY$'] . "\Repositories\\{$modelName}RepositoryEloquent;";
            $interfaceRepository = 'use ' . $this->commandData->dynamicVars['$NAMESPACE_REPOSITORY$'] . "\Interfaces\\{$modelName}Repository;";

            // checa se já não existe o binding
            if (Str::contains($providerFile, $repositoryNamespace)) {
                return false;
            }

            $templateData = str_replace($bindNamespace, implode(infy_nl_tab(1, 0), [$repositoryNamespace, $interfaceRepository, $bindNamespace]), $templateData);

            File::put($path . $fileName, $templateData);

            $this->commandData->commandInfo('Bindings created successfully.');
        } catch (FileAlreadyExistsException $e) {
            $this->commandData->commandError('Binding already exists!');
            return false;
        }
    }
}
