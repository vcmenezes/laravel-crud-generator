<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;

class PresenterGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $pathPresenter;
    private $pathTransformer;

    /** @var string */
    private $fileNamePresenter;
    private $fileNameTransformer;

    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;
        $this->pathPresenter = $commandData->config->pathPresenter;
        $this->pathTransformer = $commandData->config->pathTransformer;

        $tableName = $this->commandData->config->tableName;
        if (strpos($tableName, '.') !== false) {
            $this->pathPresenter .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
            $this->pathTransformer .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
        }

        $this->fileNamePresenter = $this->commandData->modelName . 'Presenter.php';
        $this->fileNameTransformer = $this->commandData->modelName . 'Transformer.php';
    }

    public function generate()
    {
        $templateDataPresenter = get_template('presenter', 'crud-generator');
        $templateDataTransformer = get_template('transformer', 'crud-generator');

        $templateDataTransformer = str_replace('$MODEL_RESPONSE$', implode(',' . infy_nl_tab(1, 3), $this->generateModelData()), $templateDataTransformer);
        $templateDataTransformer = str_replace('$RELATION_CONSTANTS$', implode(',' . infy_nl_tab(1, 2), $this->generateAvailableIncludes()), $templateDataTransformer);

        [$includeMethods, $includeNamespaces] = $this->generateIncludeMethods();

        $templateDataTransformer = str_replace('$NAMESPACE_INCLUDES$', implode(infy_nl_tab(1, 0), $includeNamespaces), $templateDataTransformer);
        $templateDataTransformer = str_replace('$INCLUDE_METHODS$', implode(infy_nl_tab(1, 0), $includeMethods), $templateDataTransformer);

        $templateDataPresenter = fill_template($this->commandData->dynamicVars, $templateDataPresenter);
        $templateDataTransformer = fill_template($this->commandData->dynamicVars, $templateDataTransformer);

        FileUtil::createFile($this->pathPresenter, $this->fileNamePresenter, $templateDataPresenter);
        FileUtil::createFile($this->pathTransformer, $this->fileNameTransformer, $templateDataTransformer);

        $this->commandData->commandComment("\nPresenter created: ");
        $this->commandData->commandInfo($this->fileNamePresenter);
        $this->commandData->commandInfo($this->fileNameTransformer);
    }

    private function generateModelData()
    {
        $data = [];

        foreach ($this->commandData->fields as $field) {
            $line = $this->commandData->modelName . '::' . Str::upper($field->name) . ' => ';
            $line .= "\$model->$field->name";
            $data[] = $line;
        }

        return $data;
    }

    private function generateAvailableIncludes()
    {
        $relations = [];

        foreach ($this->commandData->relations as $relation) {
            $functionName = BaseGenerator::getRelationName($relation);

            if (!empty($functionName)) {
                $const = $this->commandData->modelName . '::RELACAO_' . Str::upper(Str::snake($functionName));
                $relations[] = $const;
            }
        }

        return array_unique($relations);
    }

    private function generateIncludeMethods()
    {
        //$NAMESPACE_TRANSFORMER$\$MODEL_NAME$Transformer;
        $includeMethods = [];
        $includeNamespaces = [];
        $itemTypes = ['1t1', 'mt1'];
        $collectionTypes = ['1tm', 'mtm', 'hmt'];
        $transformNamespace = config('menezes.laravel_generator.namespace.transformer', 'App\Transformers');

        foreach ($this->commandData->relations as $relation) {
            if (in_array($relation->type, $itemTypes, true)) {
                // transformer_include_item
                $template = get_template('transformer_include_item', 'crud-generator');
                $relationName = $relation->relationNameSingular;

            } else if (in_array($relation->type, $collectionTypes, true)) {
                // transformer_include_collection
                $template = get_template('transformer_include_collection', 'crud-generator');
                $relationName = $relation->relationNamePlural;

            } else {
                $this->commandData->commandError('Erro ao identificar relacionamento!');
                exit;
            }

            $template = fill_template($this->commandData->dynamicVars, $template);
            $template = str_replace('$RELATION_NAME$', Str::ucfirst($relationName), $template);
            $template = str_replace('$RELATION_NAME_CAMEL$', Str::camel($relationName), $template);
            $template = str_replace('$RELATION_NAME_SINGULAR$', Str::ucfirst($relation->relationNameSingular), $template);

            if (!Str::contains($this->commandData->config->nsTransformer, Str::ucfirst($relation->namespace))) {
                $includeNamespaces[] = 'use ' . $transformNamespace . '\\' . Str::ucfirst($relation->namespace) . '\\' . Str::ucfirst($relation->relationNameSingular) . 'Transformer;';
            }

            $includeMethods[] = $template;
        }

        return [array_unique($includeMethods), array_unique($includeNamespaces)];
    }
}
