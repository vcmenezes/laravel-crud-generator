<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;

class ControllerGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;
    private $routesGroupPath;
    private $routeGroupContents;
    private $routeGroupTemplate;
    private $routeResourceContents;
    private $routeResourceTemplate;

    /** @var string */
    private $fileName;
    private $tableName;

    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;

        $this->path = $commandData->config->pathApiController;

        $this->tableName = $this->commandData->config->tableName;
        if (strpos($this->tableName, '.') !== false) {
            $this->path .= Str::ucfirst(explode('.', $this->tableName)[0]) . '/';
        }

        $this->fileName = $this->commandData->modelName . 'Controller.php';
    }

    public function generate()
    {
        $templateData = get_template('api.controller.api_controller', 'crud-generator');

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        FileUtil::createFile($this->path, $this->fileName, $templateData);

        $this->commandData->commandComment("\nController criado com sucesso: $this->fileName");

        $this->generateRoutes();
    }

    private function generateRoutes()
    {
        // api.php
        $this->routesGroupPath = $this->commandData->config->pathApiRoutes;

        // conteudo atual do arquivo de rotas api.php
        $this->routeGroupContents = file_get_contents($this->routesGroupPath);

        // template com o resource
        $this->routeResourceTemplate = get_template('api.routes.api_routes_resource', 'crud-generator');
        $this->routeResourceTemplate = fill_template($this->commandData->dynamicVars, $this->routeResourceTemplate);

        // checa se está em um schema para criar um arquivo separado de rotas
        if (strpos($this->tableName, '.') !== false) {
            $fileNamespace = explode('.', $this->tableName)[0];

            $routePath = 'routes/';
            $fileName = $fileNamespace . '.php';

            // checa se já existe um arquivo com o namespace correspondente ao schema
            if (!file_exists($routePath . $fileName)) {
                // cria o arquivo correspondente ao schema/namespace
                FileUtil::createFile($routePath, $fileName, "<?php\n$this->routeResourceTemplate");
            } else {
                // atualiza o arquivo correspondente
                $this->routeResourceContents = file_get_contents($routePath . $fileName);
                // checa se já não existe no arquivo
                if (!Str::contains($this->routeResourceContents, $this->routeResourceTemplate)) {
                    $this->routeResourceContents .= "\n" . $this->routeResourceTemplate;
                    file_put_contents($routePath . $fileName, $this->routeResourceContents);
                }
            }

            $this->routeGroupTemplate = get_template('api.routes.routes', 'crud-generator');
            $this->routeGroupTemplate = str_replace('$NAMESPACE_ROUTE$', Str::ucfirst($fileNamespace), $this->routeGroupTemplate);
            $this->routeGroupTemplate = str_replace('$RESOURCE_ROUTES$', $fileNamespace, $this->routeGroupTemplate);

            // checa se já não existe no arquivo
            if (!Str::contains($this->routeGroupContents, $this->routeGroupTemplate)) {
                $this->routeGroupContents .= "\n" . $this->routeGroupTemplate;
                file_put_contents($this->routesGroupPath, $this->routeGroupContents);
            }

        } else if (!Str::contains($this->routeGroupContents, $this->routeResourceTemplate)) {
            $this->routeGroupContents .= "\n" . $this->routeResourceTemplate;
            file_put_contents($this->routesGroupPath, $this->routeGroupContents);
        }
    }
}
