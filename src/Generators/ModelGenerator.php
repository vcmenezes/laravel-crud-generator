<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;
use Menezes\CrudGenerator\Utils\TableFieldsGenerator;

class ModelGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;
    private $fileName;
    private $functionNames = [];

    /**
     * ModelGenerator constructor.
     *
     * @param CommandData $commandData
     */
    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;

        $this->path = $commandData->config->pathModel;

        $tableName = $this->commandData->config->tableName;

        if (strpos($tableName, '.') !== false) {
            $this->path .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
        }

        $this->fileName = $this->commandData->modelName . '.php';
    }

    public function generate()
    {
        $templateData = get_template('model.model', 'crud-generator');

        $templateData = $this->fillTemplate($templateData);

        FileUtil::createFile($this->path, $this->fileName, $templateData);

        $this->commandData->commandComment("\nModel created: ");
        $this->commandData->commandInfo($this->fileName);
    }

    private function fillTemplate($templateData)
    {
        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        $templateData = $this->fillSoftDeletes($templateData);

        $templateData = str_replace('$CONSTANTS$', implode(infy_nl_tab(), $this->generateConstants()), $templateData);

        $templateData = str_replace('$RELATION_CONSTANTS$', implode(infy_nl_tab(), $this->generateRelationConstants()), $templateData);

        $templateData = str_replace('$FIELDS$', implode(',' . infy_nl_tab(1, 2), $this->generateFillables()), $templateData);

        $templateData = str_replace('$CAST$', implode(',' . infy_nl_tab(1, 2), $this->generateCasts()), $templateData);

        $relations = $this->generateRelations();

        $replaceRelations = fill_template($this->commandData->dynamicVars, implode(PHP_EOL . infy_nl_tab(), $relations));

        $templateData = str_replace('$RELATIONS$', $replaceRelations, $templateData);

        return $templateData;
    }

    private function fillSoftDeletes($templateData)
    {
        if (!$this->commandData->getOption('softDelete')) {
            $templateData = str_replace(array('$SOFT_DELETE_IMPORT$', '$SOFT_DELETE$', '$SOFT_DELETE_DATES$'), '', $templateData);
        } else {
            $templateData = str_replace(array('$SOFT_DELETE_IMPORT$', '$SOFT_DELETE$'),
                array("use Illuminate\\Database\\Eloquent\\SoftDeletes;" . infy_nl_tab(), 'use SoftDeletes;' . infy_nl_tab(2)),
                $templateData);
        }

        return $templateData;
    }

    private function generateFillables()
    {
        $fillables = [];

        foreach ($this->commandData->fields as $field) {
            if ($field->isFillable) {
                $fillables[] = 'self::' . Str::upper($field->name);
            }
        }

        return $fillables;
    }

    private function generateRelations()
    {
        $relations = [];
        foreach ($this->commandData->relations as $relation) {
            [$functionName, $relationText] = $relation->getRelationFunctionText();
            // checa por métodos duplicados
            if (!empty($relationText) && !in_array($functionName, $this->functionNames, true)) {
                $this->functionNames[] = $functionName;
                $relations[] = $relationText;
            }
        }

        return $relations;
    }

    private function generateCasts()
    {
        $casts = [];

        $timestamps = TableFieldsGenerator::getTimestampFieldNames();

        foreach ($this->commandData->fields as $field) {
            if (in_array($field->name, $timestamps, true)) {
                continue;
            }

            $rule = 'self::' . Str::upper($field->name) . ' => ';

            switch (strtolower($field->fieldType)) {
                case 'integer':
                case 'increments':
                case 'smallinteger':
                case 'long':
                case 'biginteger':
                    $rule .= "'integer'";
                    break;
                case 'double':
                    $rule .= "'double'";
                    break;
                case 'float':
                case 'decimal':
                    $rule .= "'float'";
                    break;
                case 'boolean':
                    $rule .= "'boolean'";
                    break;
                case 'datetime':
                case 'datetimetz':
                    $rule .= "'datetime'";
                    break;
                case 'date':
                    $rule .= "'date'";
                    break;
                case 'enum':
                case 'string':
                case 'char':
                case 'text':
                    $rule .= "'string'";
                    break;
                default:
                    $rule = '';
                    break;
            }

            if (!empty($rule)) {
                $casts[] = $rule;
            }
        }

        return $casts;
    }

    private function generateConstants()
    {
        $constants = [];

        foreach ($this->commandData->fields as $field) {
            $const = 'public const ' . Str::upper($field->name) . " = '$field->name';";

            if (!empty($const)) {
                $constants[] = $const;
            }
        }

        foreach ($this->commandData->relations as $relation) {
            if (isset($relation->inputs[1]) && Str::contains($relation->inputs[1], 'id_')) {
                $foreignKey = $relation->inputs[1];
                $const = 'public const ' . Str::upper($foreignKey) . " = '$foreignKey';";
                $constants[] = $const;
            }
        }

        return array_unique($constants);
    }

    private function generateRelationConstants()
    {
        $relations = [];

        foreach ($this->commandData->relations as $relation) {
            $functionName = BaseGenerator::getRelationName($relation);

            if (!empty($functionName)) {
                $const = 'public const RELACAO_' . Str::upper(Str::snake($functionName)) . " = '$functionName';";
                $relations[] = $const;
            }
        }

        return array_unique($relations);
    }
}
