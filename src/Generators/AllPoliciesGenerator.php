<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllPoliciesGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generatePolicies()
    {
        $this->generate('create:api_policy');
    }
}
