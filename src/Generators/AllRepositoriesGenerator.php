<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllRepositoriesGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generateRepositories()
    {
        $this->generate('create:api_repository');
    }
}
