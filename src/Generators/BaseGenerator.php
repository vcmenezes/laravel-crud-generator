<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Support\Str;

class BaseGenerator
{
    public static function getRelationName($relation)
    {
        if (!empty($relation->inputs[0])) {
            $singularRelation = Str::camel($relation->inputs[0]);
            $pluralRelation = Str::camel(Str::plural($relation->inputs[0]));
        } else {
            return null;
        }

        if (strpos($singularRelation, '.') !== false) {
            $singularRelation = explode('.', $singularRelation)[1];
            $pluralRelation = explode('.', $pluralRelation)[1];
        }

        switch ($relation->type) {
            case 'mt1':
            case '1t1':
                $functionName = $singularRelation;
                break;
            case 'mtm':
            case 'hmt':
            case '1tm':
                $functionName = $pluralRelation;
                break;
            default:
                $functionName = '';
                break;
        }

        return $functionName;
    }
}
