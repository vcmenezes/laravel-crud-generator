<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllControllersGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generateControllers()
    {
        $this->generate('create:api_controller');
    }
}
