<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;

class MigrationGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;
    private $fileName;
    private $tableName;

    public function __construct($commandData)
    {
        $this->commandData = $commandData;

        $this->path = config('menezes.laravel_generator.path.migration', base_path('database/migrations/'));

        $this->tableName = $this->commandData->config->tableName;
        if (strpos($this->tableName, '.') !== false) {
            $this->path .= Str::ucfirst(explode('.', $this->tableName)[0]) . '/';
        }

        $this->fileName = date('Y_m_d_His') . '_' . 'create_' . Str::replaceFirst('.','_', $this->tableName) . '_table.php';
    }

    public function generate()
    {
        $templateData = get_template('migration', 'crud-generator');

        $templateData = str_replace('$TABLE_NAME_TITLE$', $this->generateTableTitle($this->tableName), $templateData);

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        $templateData = str_replace('$FIELDS$', $this->generateFields(), $templateData);

        FileUtil::createFile($this->path, $this->fileName, $templateData);

        $this->commandData->commandComment("\nMigration created: ");
        $this->commandData->commandInfo($this->fileName);
    }

    private function generateFields()
    {
        $fields = [];
        $foreignKeys = [];
        $createdAtField = null;
        $updatedAtField = null;

        foreach ($this->commandData->fields as $field) {
            if ($field->name === 'created_at') {
                $createdAtField = $field;
                continue;
            }

            if ($field->name === 'updated_at') {
                $updatedAtField = $field;
                continue;
            }

            $fields[] = $field->migrationText;
            if (!empty($field->foreignKeyText)) {
                $foreignKeys[] = $field->foreignKeyText;
            }
        }

        if ($createdAtField && $updatedAtField) {
            $fields[] = '$table->timestamps();';
        } else {
            if ($createdAtField) {
                $fields[] = $createdAtField->migrationText;
            }
            if ($updatedAtField) {
                $fields[] = $updatedAtField->migrationText;
            }
        }

        if ($this->commandData->getOption('softDelete')) {
            $fields[] = '$table->softDeletes();';
        }

        return implode(infy_nl_tab(1, 3), array_merge($fields, $foreignKeys));
    }

    private function generateTableTitle($tableName)
    {
        if (strpos($tableName, '.') !== false) {
            $tableTitle = explode('.', $tableName);
            $tableTitle = Str::studly(Str::ucfirst($tableTitle[0]) . Str::ucfirst($tableTitle[1]));
        } else {
            $tableTitle = Str::studly(Str::ucfirst($tableName));
        }
        return $tableTitle;
    }
}