<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllModelsGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generateModels()
    {
        $this->generate('create:api_model');
    }
}
