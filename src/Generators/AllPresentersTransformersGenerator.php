<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllPresentersTransformersGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generatePresentersTransformers()
    {
        $this->generate('create:api_presenter');
    }
}
