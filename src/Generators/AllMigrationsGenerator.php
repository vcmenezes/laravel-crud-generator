<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Console\Command;

class AllMigrationsGenerator extends AllBaseGenerator
{
    public function __construct(Command $command)
    {
        parent::__construct($command);
    }

    public function generateMigrations()
    {
        $this->generate('create:api_migration');
    }
}
