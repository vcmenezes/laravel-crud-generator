<?php

namespace Menezes\CrudGenerator\Generators;

use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;

class ServiceGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;

    /** @var string */
    private $fileName;

    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;
        $this->path = $commandData->config->pathService;

        $tableName = $this->commandData->config->tableName;
        if (strpos($tableName, '.') !== false) {
            $this->path .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
        }

        $this->fileName = $this->commandData->modelName.'Service.php';
    }

    public function generate()
    {
        $templateData = get_template('service', 'crud-generator');

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        FileUtil::createFile($this->path, $this->fileName, $templateData);

        $this->commandData->commandComment("\nService created: ");
        $this->commandData->commandInfo($this->fileName);
    }
}
