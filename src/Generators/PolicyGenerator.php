<?php

namespace Menezes\CrudGenerator\Generators;

use Exception;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Utils\FileUtil;

class PolicyGenerator
{
    /** @var CommandData */
    private $commandData;

    /** @var string */
    private $path;

    /** @var string */
    private $fileName;

    public function __construct(CommandData $commandData)
    {
        $this->commandData = $commandData;
        $this->path = $commandData->config->pathPolicy;

        $tableName = $this->commandData->config->tableName;

        if (strpos($tableName, '.') !== false) {
            $this->path .= Str::ucfirst(explode('.', $tableName)[0]) . '/';
        }

        $this->fileName = $this->commandData->modelName . 'Policy.php';
    }

    public function generate()
    {
        $templateData = get_template('policy', 'crud-generator');

        $templateData = fill_template($this->commandData->dynamicVars, $templateData);

        $permissoes = $this->generateDireitos();

        $this->commandData->commandComment("\nArquivo de direitos criado ou atualizado!");

        if ($permissoes !== false) {
            $templateData = str_replace('$SHOW_PERMISSAO$', 'Direitos::' . $permissoes['show'], $templateData);
            $templateData = str_replace('$STORE_PERMISSAO$', 'Direitos::' . $permissoes['store'], $templateData);
            $templateData = str_replace('$UPDATE_PERMISSAO$', 'Direitos::' . $permissoes['update'], $templateData);
            $templateData = str_replace('$DESTROY_PERMISSAO$', 'Direitos::' . $permissoes['destroy'], $templateData);
            $templateData = str_replace('$SHOW_LOGS_PERMISSAO$', 'Direitos::' . $permissoes['show_logs'], $templateData);
            $templateData = str_replace('$SHOW_DELETEDS_PERMISSAO$', 'Direitos::' . $permissoes['show_deleteds'], $templateData);
        }

        FileUtil::createFile($this->path, $this->fileName, $templateData);

        $this->updateAuthServiceProvider($this->commandData->modelName);

        $this->commandData->commandComment("\nPolicy criada: ");
        $this->commandData->commandInfo($this->fileName);
    }

    private function generateDireitos()
    {
        try {
            // generate repository service provider
            $templateData = get_template('direitos', 'crud-generator');

            $direitosPlaceholder = '//:direitos:';

            $path = config('menezes.laravel_generator.path.direitos', config_path('direitos/'));
            $fileName = 'Direitos.php';

            if (!file_exists($path . $fileName)) {
                FileUtil::createFile($path, $fileName, $templateData);
            }

            $direitosFile = File::get($path . $fileName);
            $tableName = $this->commandData->config->tableName;

            [$constantes, $permissoes] = $this->generateConstantesDireito($tableName);

            // checa se já não existe o binding
            if (Str::contains($direitosFile, $constantes)) {
                return false;
            }

            $constantes[] = $direitosPlaceholder;

            $templateData = str_replace($direitosPlaceholder, implode(infy_nl_tab(1, 1), $constantes), $direitosFile);

            File::put($path . $fileName, $templateData);

            $this->commandData->commandInfo('Direitos created successfully.');

            return $permissoes;
        } catch (Exception $e) {
            $this->commandData->commandError($e->getMessage());
            return false;
        }
    }

    private function generateConstantesDireito($tableName)
    {
        $constantes = [];
        $permissoes = [];
        $constDeclaration = 'public const ';
        $crudMethods = ['show', 'store', 'update', 'destroy', 'show_logs', 'show_deleteds'];

        // checa se está em um schema
        if (strpos($tableName, '.') !== false) {
            [$schema, $table] = explode('.', $tableName);
            $constBase = Str::upper($schema) . '_' . Str::upper($table);
        } else {
            $constBase = Str::upper($tableName);
        }

        $constantes[] = "$constDeclaration$constBase = '$constBase';";

        foreach ($crudMethods as $method) {
            $const = $constBase . '_' . Str::upper($method);
            $constantes[] = $constDeclaration . "$const = '$const';";
            $permissoes[$method] = $const;
        }

        return [$constantes, $permissoes];
    }

    private function updateAuthServiceProvider($modelName)
    {
        // 'App\Model' => 'App\Policies\ModelPolicy',
        $policyPlaceholder = "// 'App\Model' => 'App\Policies\ModelPolicy',";
        $namespacePlaceholder = "namespace App\Providers;";

        $path = config('menezes.laravel_generator.path.provider', app_path('Providers/'));
        $fileName = 'AuthServiceProvider.php';

        if (!file_exists($path . $fileName)) {
            $this->commandData->commandError('AuthServiceProvider não encontrado');
            return false;
        }

        $providerFile = File::get($path . $fileName);

        $modelClass = $modelName . '::class';
        $policyClass = $modelName . 'Policy::class';
        $replace = $modelClass . ' => ' . $policyClass . ',' . infy_nl_tab(1, 2) . $policyPlaceholder;

        $templateData = str_replace($policyPlaceholder, $replace, $providerFile);

        $policyNamespace = 'use ' . $this->commandData->dynamicVars['$NAMESPACE_POLICY$'] . "\\{$modelName}Policy;";
        $modelNamespace = 'use ' . $this->commandData->dynamicVars['$NAMESPACE_MODEL$'] . "\\{$modelName};";

        if (Str::contains($providerFile, $policyNamespace)) {
            $this->commandData->commandInfo('AuthServiceProvider já possui declaração da policy!');
            return false;
        }

        $templateData = str_replace($namespacePlaceholder, implode(infy_nl_tab(1, 0), [$namespacePlaceholder . "\n", $policyNamespace, $modelNamespace]), $templateData);

        File::put($path . $fileName, $templateData);

        $this->commandData->commandInfo('AuthServiceProvider atualizado!');
    }
}