<?php

namespace Menezes\CrudGenerator\Commands;

use Menezes\CrudGenerator\Common\CommandData;

class APIServiceGenerator extends BaseCommand
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'create:api_service';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gera um Service através da leitura de uma tabela específica.';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();

        $this->commandData = new CommandData($this);
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        parent::handle();

        $this->generateService();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return array_merge(parent::getOptions(), []);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array_merge(parent::getArguments(), []);
    }
}
