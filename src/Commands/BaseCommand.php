<?php


namespace Menezes\CrudGenerator\Commands;


use Doctrine\DBAL\DBALException;
use Illuminate\Console\Command;
use Illuminate\Support\Composer;
use Illuminate\Support\Str;
use Menezes\CrudGenerator\Common\CommandData;
use Menezes\CrudGenerator\Generators\AllControllersGenerator;
use Menezes\CrudGenerator\Generators\AllMigrationsGenerator;
use Menezes\CrudGenerator\Generators\AllModelsGenerator;
use Menezes\CrudGenerator\Generators\AllPoliciesGenerator;
use Menezes\CrudGenerator\Generators\AllPresentersTransformersGenerator;
use Menezes\CrudGenerator\Generators\AllRepositoriesGenerator;
use Menezes\CrudGenerator\Generators\AllRequestsGenerator;
use Menezes\CrudGenerator\Generators\AllServicesGenerator;
use Menezes\CrudGenerator\Generators\ControllerGenerator;
use Menezes\CrudGenerator\Generators\MigrationGenerator;
use Menezes\CrudGenerator\Generators\ModelGenerator;
use Menezes\CrudGenerator\Generators\PolicyGenerator;
use Menezes\CrudGenerator\Generators\PresenterGenerator;
use Menezes\CrudGenerator\Generators\RepositoryGenerator;
use Menezes\CrudGenerator\Generators\RequestGenerator;
use Menezes\CrudGenerator\Generators\ServiceGenerator;
use Menezes\CrudGenerator\Utils\FileUtil;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class BaseCommand extends Command
{
    /**
     * The command Data.
     *
     * @var CommandData
     */
    public $commandData;

    /**
     * @var Composer
     */
    public $composer;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
        // Pra usar o dump-autoload
        $this->composer = app()['composer'];
    }

    public function handle()
    {
        $this->commandData->modelName = Str::studly($this->argument('model'));
        $this->commandData->initCommandData();
    }

    /**
     * @throws DBALException
     */
    public function generateModel()
    {
        $this->commandData->getFields();
        $modelGenerator = new ModelGenerator($this->commandData);
        $modelGenerator->generate();
    }

    public function generateAllModelsFromDatabase()
    {
        $allModelsGenerator = new AllModelsGenerator($this);
        $allModelsGenerator->generateModels();
    }

    /**
     * @throws DBALException
     */
    public function generateMigration()
    {
        $this->commandData->getFields();
        $migrationGenerator = new MigrationGenerator($this->commandData);
        $migrationGenerator->generate();
    }

    public function generateAllMigrationsFromDatabase()
    {
        $allMigrationsGenerator = new AllMigrationsGenerator($this);
        $allMigrationsGenerator->generateMigrations();
    }

    public function generateController()
    {
        $controllerGenerator = new ControllerGenerator($this->commandData);
        $controllerGenerator->generate();
    }

    public function generateAllControllersFromDatabase()
    {
        $allControllersGenerator = new AllControllersGenerator($this);
        $allControllersGenerator->generateControllers();
    }

    public function generateRepository()
    {
        $repositoryGenerator = new RepositoryGenerator($this->commandData);
        $repositoryGenerator->generate();
    }

    public function generateAllRepositoriesFromDatabase()
    {
        $allRepositoriesGenerator = new AllRepositoriesGenerator($this);
        $allRepositoriesGenerator->generateRepositories();
    }

    public function generateService()
    {
        $serviceGenerator = new ServiceGenerator($this->commandData);
        $serviceGenerator->generate();
    }

    public function generateAllServicesFromDatabase()
    {
        $allServicesFromDatabase = new AllServicesGenerator($this);
        $allServicesFromDatabase->generateServices();
    }

    public function generateRequests()
    {
        $this->commandData->getFields();
        $requestGenerator = new RequestGenerator($this->commandData);
        $requestGenerator->generate();
    }

    public function generateAllRequestsFromDatabase()
    {
        $allRequestsFromDatabase = new AllRequestsGenerator($this);
        $allRequestsFromDatabase->generateRequests();
    }

    public function generatePresenterTransformer()
    {
        $this->commandData->getFields();
        $presenterGenerator = new PresenterGenerator($this->commandData);
        $presenterGenerator->generate();
    }

    public function generateAllPresentersTransformersFromDatabase()
    {
        $allPresentersTransformers = new AllPresentersTransformersGenerator($this);
        $allPresentersTransformers->generatePresentersTransformers();
    }

    public function generatePolicy()
    {
        $policyGenerator = new PolicyGenerator($this->commandData);
        $policyGenerator->generate();
    }

    public function generateAllPoliciesFromDatabase()
    {
        $allPolicies = new AllPoliciesGenerator($this);
        $allPolicies->generatePolicies();
    }

    /**
     * @param $fileName
     * @param string $prompt
     *
     * @return bool
     */
    protected function confirmOverwrite($fileName, $prompt = '')
    {
        $prompt = empty($prompt)
            ? $fileName . ' already exists. Do you want to overwrite it? [y|N]'
            : $prompt;

        return $this->confirm($prompt);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return [
            ['tableName', null, InputOption::VALUE_REQUIRED, 'Table Name'],
            ['fromTable', null, InputOption::VALUE_NONE, 'Generate from existing table'],
            ['ignoreFields', null, InputOption::VALUE_REQUIRED, 'Ignore fields while generating from table'],
            ['softDelete', null, InputOption::VALUE_NONE, 'Soft Delete Option'],
            ['plural', null, InputOption::VALUE_REQUIRED, 'Plural Model name'],
        ];
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['model', InputArgument::OPTIONAL, 'Singular Model name'],
        ];
    }
}
