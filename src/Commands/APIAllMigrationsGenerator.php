<?php

namespace Menezes\CrudGenerator\Commands;

use Menezes\CrudGenerator\Common\CommandData;

class APIAllMigrationsGenerator extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'create:api_migrations';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gera Migrations através da leitura de todas as tabelas do banco de dados.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->commandData = new CommandData($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();

        $this->generateAllMigrationsFromDatabase();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return array_merge(parent::getOptions(), []);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array_merge(parent::getArguments(), []);
    }
}
