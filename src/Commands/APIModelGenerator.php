<?php

namespace Menezes\CrudGenerator\Commands;

use Doctrine\DBAL\DBALException;
use Menezes\CrudGenerator\Common\CommandData;

class APIModelGenerator extends BaseCommand
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'create:api_model';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gera um Model através da leitura de uma tabela específica.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->commandData = new CommandData($this);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     * @throws DBALException
     */
    public function handle()
    {
        parent::handle();

        $this->generateModel();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    public function getOptions()
    {
        return array_merge(parent::getOptions(), []);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array_merge(parent::getArguments(), []);
    }
}
