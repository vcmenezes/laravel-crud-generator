<?php

namespace Menezes\CrudGenerator\Traits;

use Ramsey\Uuid\Uuid as UUU;

trait Uuid
{
	public static function bootUuid()
	{
		static::creating(function ($model) {
			$model->uuid = UUU::uuid4();
		});
	}
}