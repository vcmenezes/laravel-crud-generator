<?php

namespace Menezes\CrudGenerator\Traits;

use Illuminate\Http\Request;
use League\Fractal\TransformerAbstract;

trait CheckIncludeRequest
{
    public function getRelationsFromRequest(TransformerAbstract $transformer, Request $request)
    {
        // Retorna os includes disponiveis no transformer
        $availableIncludes = $transformer->getAvailableIncludes();
        // Pega as relações passadas na url. Ex: conta,pessoa,etc
        $includes = explode(',', $request->get('include'));
        // Checa se foi passada alguma relação mais profunda. Ex: historicos.tipoRestricao
        $relationsArray = [];
        foreach ($includes as $include) {
            $relations = explode('.', $include);
            if (in_array($relations[0], $availableIncludes, true)) {
                $relationsArray[] = $include;
            }
        }
        return $relationsArray;
    }
}
