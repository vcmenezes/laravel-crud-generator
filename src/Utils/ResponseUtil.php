<?php

namespace Menezes\CrudGenerator\Utils;

class ResponseUtil
{
    /**
     * @param $service
     * @param $result
     * @param string $message
     * @return array
     */
    public static function makeResponse(BaseServiceMethods $service, $result = null, $message = null)
    {
        $service->setPresenter();
        $result = $service->getTransformedResponse($result);

        if (request()->get('filter')) {
            $filterFields = explode(';', request()->get('filter'));
            foreach ($result['data'] as $key => $value) {
                foreach ($value as $k => $val) {
                    if (!in_array($k, $filterFields, true)) {
                        unset($result['data'][$key][$k]);
                    }
                }
            }
        }

        $res = [
            'data' => $result['data'] ?: null
        ];
        if (!empty($message)) {
            $res['message'] = $message;
        }
        if (!empty($result['meta'])) {
            $res['meta'] = $result['meta'];
        }
        return $res;
    }

    /**
     * @param string $message
     * @param array $errors
     *
     * @return array
     */
    public static function makeError($message = null, array $errors = [])
    {
        return [
            'message' => $message,
            'errors' => $errors
        ];
    }
}
