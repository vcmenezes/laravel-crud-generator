<?php

namespace Menezes\CrudGenerator\Utils;

use App\Models\Seguranca\Usuario;
use Exception;
use Illuminate\Database\Eloquent\RelationNotFoundException;
use Illuminate\Foundation\Auth\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Menezes\CrudGenerator\Criteria\GetSoftDeletedRegistersCriteria;
use Menezes\CrudGenerator\Traits\CheckIncludeRequest;
use Netpok\Database\Support\DeleteRestrictionException;
use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Exceptions\RepositoryException;
use Prettus\Repository\Presenter\FractalPresenter;

class BaseServiceMethods
{
    use CheckIncludeRequest;

    /** @var BaseRepository $repository */
    protected $repository;

    /** @var FractalPresenter $presenter */
    protected $presenter;

    /** @var User */
    protected $user;

    public function __construct()
    {
        //TODO remover depois, so para teste
        Auth::login(Usuario::first());
        $this->user = Auth::user() ?: abort(401, 'Usuário não autenticado!');
    }

    /**
     * @param int $id
     * @return mixed
     * @throws RepositoryException
     */
    public function getOne(int $id)
    {
        $this->checkIfUserHasPermissao('show', $this->repository->makeModel());

        return $this->repository->pushCriteria(new GetSoftDeletedRegistersCriteria(request()))->find($id);
    }

    /**
     * @param Request $request
     * @return mixed
     * @throws RepositoryException
     */
    public function getAll(Request $request)
    {
        $this->checkIfUserHasPermissao('show', $this->repository->makeModel());

        $this->repository->pushCriteria(new GetSoftDeletedRegistersCriteria($request));

        $relations = $this->getRelationsFromRequest($this->presenter->getTransformer(), $request);
        if (!empty($relations)) {
            try {
                return $this->repository->with($relations)->paginate($request->query('limit'));
            } catch (RelationNotFoundException $exception) {
                // Alguma relação inválida foi passada na url
                throw new RelationNotFoundException(__('error.invalid.include', ['exception' => $exception->getMessage()]));
            }
        } else {
            return $this->repository->paginate($request->query('limit'));
        }
    }

    /**
     * @param array $input
     * @return mixed
     * @throws Exception
     */
    public function create(array $input)
    {
        $this->checkIfUserHasPermissao('store', $this->repository->model());

        try {
            return $this->repository->create($input);
        } catch (Exception $exception) {
            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param array $inputs
     * @return mixed
     * @throws Exception
     */
    public function createMany(array $inputs)
    {
        $this->checkIfUserHasPermissao('store', $this->repository->model());

        DB::beginTransaction();

        $createdIds = array();

        try {
            foreach ($inputs as $input) {
                $createdIds[] = ($this->create($input))['data']['id'];
            }

            DB::commit();

            return $this->repository->findWhereIn('id', $createdIds);
        } catch (Exception $exception) {
            DB::rollBack();

            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param array $input
     * @param int $id
     * @return mixed
     * @throws Exception
     */
    public function update(array $input, int $id)
    {
        $this->checkIfUserHasPermissao('update', $this->repository->makeModel());

        try {
            return $this->repository->update($input, $id);
        } catch (Exception $exception) {
            throw new Exception(__('error.update') . $exception->getMessage());
        }
    }

    /**
     * @param array $inputs
     * @return mixed
     * @throws Exception
     */
    public function updateMany(array $inputs)
    {
        $this->checkIfUserHasPermissao('update', $this->repository->makeModel());

        DB::beginTransaction();

        $updatedIds = array();

        try {
            foreach ($inputs as $input) {
                $id = $input['id'];

                // Remove o id do array
                unset($input['id']);

                $updatedIds[] = $id;

                $this->update($input, $id);
            }

            DB::commit();

            return $this->repository->findWhereIn('id', $updatedIds);
        } catch (Exception $exception) {
            DB::rollBack();

            throw new Exception($exception->getMessage());
        }
    }

    /**
     * @param int $id
     * @return int
     * @throws RepositoryException
     */
    public function delete(int $id)
    {
        $this->checkIfUserHasPermissao('destroy', $this->repository->makeModel());

        return $this->repository->delete($id);
    }

    /**
     * @param array $inputs
     * @return mixed
     * @throws Exception || DeleteRestrictionException
     */
    public function deleteMany(array $inputs)
    {
        $this->checkIfUserHasPermissao('destroy', $this->repository->makeModel());

        DB::beginTransaction();

        $currentId = null;

        try {
            foreach ($inputs as $input) {
                $currentId = $input['id'];

                $this->delete($currentId);
            }

            DB::commit();

            return true;
        } catch (Exception $exception) {
            DB::rollBack();

            if ($exception instanceof DeleteRestrictionException) {
                throw new DeleteRestrictionException($exception->getMessage() . " Model ID: $currentId");
            }
            throw new Exception($exception->getMessage());
        }
    }

    public function setPresenter()
    {
        $this->repository->setPresenter($this->presenter);
    }

    public function getTransformedResponse($result)
    {
        return $this->repository->parserResult($result);
    }

    private function checkIfUserHasPermissao($permissao, $model)
    {
        abort_if($this->user->cant($permissao, $model), 403, 'Usuário não tem permissão para acessar este recurso!');
    }
}
