<?php


namespace Menezes\CrudGenerator\Utils;

use function array_key_exists;
use function is_array;

class ArrayUtil
{
    public static function isMultiArray($input)
    {
        return !empty(array_filter($input, function ($e) {
            return is_array($e);
        }));
    }

    public static function checkKeyExists(array $arr, $key)
    {
        // is in base array?
        if (array_key_exists($key, $arr)) {
            return true;
        }

        // check arrays contained in this array
        foreach ($arr as $element) {
            if (is_array($element) && self::checkKeyExists($element, $key)) {
                return true;
            }
        }
        return false;
    }
}
