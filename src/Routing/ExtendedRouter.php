<?php

namespace Menezes\CrudGenerator\Routing;

use Illuminate\Routing\ResourceRegistrar;

class ExtendedRouter extends ResourceRegistrar
{
    protected $resourceDefaults = ['index', 'store', 'storeMany', 'show', 'update', 'updateMany', 'destroy', 'destroyMany'];

    protected function addResourceStoreMany($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name) . '/list';

        $action = $this->getResourceAction($name, $controller, 'storeMany', $options);

        return $this->router->post($uri, $action);
    }

    protected function addResourceUpdateMany($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name) . '/list';

        $action = $this->getResourceAction($name, $controller, 'updateMany', $options);

        return $this->router->put($uri, $action);
    }

    protected function addResourceDestroyMany($name, $base, $controller, $options)
    {
        $uri = $this->getResourceUri($name) . '/list';

        $action = $this->getResourceAction($name, $controller, 'destroyMany', $options);

        return $this->router->delete($uri, $action);
    }
}
