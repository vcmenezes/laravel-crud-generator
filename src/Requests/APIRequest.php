<?php

namespace Menezes\CrudGenerator\Requests;

use Menezes\CrudGenerator\Utils\ResponseUtil;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Response;

/**
 * Class APIRequest
 * @package App\Http\Requests
 */
class APIRequest extends FormRequest
{
    /**
     * @param Validator $validator
     */
    protected function failedValidation(Validator $validator)
    {
        $message = method_exists($this, 'message')
            ? $this->container->call([$this, 'message'])
            : 'The given data was invalid.';

        throw new HttpResponseException(Response::json(ResponseUtil::makeError($message, $validator->errors()->toArray()), 422));
    }
}
