<?php

namespace Menezes\CrudGenerator;

use Illuminate\Routing\ResourceRegistrar;
use Illuminate\Support\ServiceProvider;
use Menezes\CrudGenerator\Commands\APIAllControllersGenerator;
use Menezes\CrudGenerator\Commands\APIAllMigrationsGenerator;
use Menezes\CrudGenerator\Commands\APIAllModelsGenerator;
use Menezes\CrudGenerator\Commands\APIAllPoliciesGenerator;
use Menezes\CrudGenerator\Commands\APIAllPresentersTransformers;
use Menezes\CrudGenerator\Commands\APIAllRepositoriesGenerator;
use Menezes\CrudGenerator\Commands\APIAllRequestsGenerator;
use Menezes\CrudGenerator\Commands\APIAllServicesGenerator;
use Menezes\CrudGenerator\Commands\APIControllerGenerator;
use Menezes\CrudGenerator\Commands\APIMigrationGenerator;
use Menezes\CrudGenerator\Commands\APIModelGenerator;
use Menezes\CrudGenerator\Commands\APIPolicyGenerator;
use Menezes\CrudGenerator\Commands\APIPresenterTransformerGenerator;
use Menezes\CrudGenerator\Commands\APIRepositoryGenerator;
use Menezes\CrudGenerator\Commands\APIRequestGenerator;
use Menezes\CrudGenerator\Commands\APIServiceGenerator;
use Menezes\CrudGenerator\Routing\ExtendedRouter;

class CrudGeneratorServiceProvider extends ServiceProvider
{
    protected $commands = [
        APIModelGenerator::class,
        APIAllModelsGenerator::class,
        APIMigrationGenerator::class,
        APIAllMigrationsGenerator::class,
        APIControllerGenerator::class,
        APIAllControllersGenerator::class,
        APIRepositoryGenerator::class,
        APIAllRepositoriesGenerator::class,
        APIRequestGenerator::class,
        APIAllRequestsGenerator::class,
        APIServiceGenerator::class,
        APIAllServicesGenerator::class,
        APIPresenterTransformerGenerator::class,
        APIAllPresentersTransformers::class,
        APIPolicyGenerator::class,
        APIAllPoliciesGenerator::class
    ];

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->runningInConsole()) {
            $this->commands($this->commands);
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerHelpers();

        $configPath = __DIR__ . '/../config/laravel_generator.php';

        $this->publishes([
            $configPath => config_path('menezes/laravel_generator.php'),
        ]);

        $this->app->bind(ResourceRegistrar::class, ExtendedRouter::class);
    }

    /**
     * Register helpers file
     */
    public function registerHelpers()
    {
        // Load the helpers
        if (file_exists($file = __DIR__ . '/helpers.php')) {
            require $file;
        }
    }
}
