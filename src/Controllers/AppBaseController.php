<?php

namespace Menezes\CrudGenerator\Controllers;

use App\Http\Controllers\Controller;
use Menezes\CrudGenerator\Utils\BaseServiceMethods;
use Menezes\CrudGenerator\Utils\ResponseUtil;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Response;

/**
 * Class AppBaseController
 * @package App\Http\Controllers
 */
class AppBaseController extends Controller
{
    /**
     * @param $service
     * @param $result
     * @param $message
     * @param int $code
     * @return JsonResponse
     */
    public function sendResponse(BaseServiceMethods $service, $result = null, $message = null, $code = null): JsonResponse
    {
        if($code === null){
            $code = $this->getCodeByCallerFunction(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS,2)[1]['function']);
        }

		return Response::json(ResponseUtil::makeResponse($service, $result, $message), $code);
	}

    /**
     * @param $error
     * @param int $code
     * @return JsonResponse
     */
    public function sendError($error, $code = 404): JsonResponse
    {
		return Response::json(ResponseUtil::makeError($error), $code);
	}

    public function getCodeByCallerFunction($callerFunction)
    {
        if ($callerFunction === 'store' || $callerFunction === 'storeMany') {
            return 201;
        }
        return 200;
    }
}
