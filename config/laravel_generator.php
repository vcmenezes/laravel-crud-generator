<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Paths
    |--------------------------------------------------------------------------
    |
    */

    'path' => [

        'migration' => base_path('database/migrations/'),

        'model' => app_path('Models/'),

        'provider' => app_path('Providers/'),

        'repository' => app_path('Repositories/'),

        'service' => app_path('Services/'),

        'presenter' => app_path('Presenters/'),

        'transform' => app_path('Transforms/'),

        'routes' => base_path('routes/api.php'),

        'request' => app_path('Http/Requests/'),

        'controller' => app_path('Http/Controllers/'),

        'policy' => app_path('Policies/'),

        'user_model' => app_path('Models/Seguranca')
    ],

    /*
    |--------------------------------------------------------------------------
    | Namespaces
    |--------------------------------------------------------------------------
    |
    */

    'namespace' => [

        'model' => 'App\Models',

        'repository' => 'App\Repositories',

        'service' => 'App\Services',

        'presenter' => 'App\Presenters',

        'transform' => 'App\Transforms',

        'controller' => 'App\Http\Controllers',

        'request' => 'App\Http\Requests',

        'policy' => 'App\Policies',

        'user_model' => 'App\Models\Seguranca'
    ],

    /*
    |--------------------------------------------------------------------------
    | Model extend class
    |--------------------------------------------------------------------------
    |
    */

    'model_extend_class' => 'Illuminate\Database\Eloquent\Model',
    'model_user' => 'Usuario',

    /*
    |--------------------------------------------------------------------------
    | Options
    |--------------------------------------------------------------------------
    |
    */

    'options' => [
        'softDelete' => true
    ],

    /*
    |--------------------------------------------------------------------------
    | Timestamp Fields
    |--------------------------------------------------------------------------
    |
    */

    'timestamps' => [

        'enabled' => true,

        'created_at' => 'created_at',

        'updated_at' => 'updated_at',

        'deleted_at' => 'deleted_at',
    ],

    /*
    |--------------------------------------------------------------------------
    | Specify custom doctrine mappings as per your need
    |--------------------------------------------------------------------------
    |
    */
    'from_table' => [

        'doctrine_mappings' => [],
    ],

];
